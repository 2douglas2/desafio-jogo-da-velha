//Javascript_JogoDaVelha

//variável que recebe o jogador da vez
vez = "X"

// Variável que recbe a quantidade jogadas
var cont = 0

// Realiza uma jogada
function selecionar(botao) {

    //variável que recebe a casa que foi marcada    
    var N = botao

    //Virifica se a casa pode ser jogada, se já existe um vencedor ou se deu velha e realiza a jogada
    if(document.getElementsByClassName("casa")[N].innerHTML != "X" && document.getElementsByClassName("casa")[N].innerHTML != "O" && ganhou()==0 && cont !=9){
        //Casa recebe a jogada 
        document.getElementsByClassName("casa")[N].innerHTML = vez

        //Quantidade de jogadas soma +1
        cont++
        
        //Verifica se a jogada resultou em um vencedor e mosta na tela quem veceu
        if (ganhou() !=0){
            document.getElementById("IndicadorVencedor").innerHTML = vez + " venceu"
        }

        //Verifica se foram feitas 9 jogadas e ninguém venceu e mostra na tela "Velha"
        if (cont==9 && ganhou()==0){
            document.getElementById("IndicadorVencedor").innerHTML = "Velha"
        }

        //Altera o jogador na variável vez
        if (vez == "X"){
            vez = "O"
            document.getElementById("IndicadorDaVez").innerHTML = "O"
        } 
        else{
            vez = "X"
            document.getElementById("IndicadorDaVez").innerHTML = "X"
        }
        
    }

}

// Zera todos as posições e recomeça o jogo
function resetar(){

    //Volta o jogo para o padrão inicial
    for(var i=0; i<9;i++){
        //Limpa o campo
        document.getElementsByClassName("casa")[i].innerHTML = ""
        
        //Vez recebe jogador X
        vez = "X"

        //Mostra que a vez é do X
        document.getElementById("IndicadorDaVez").innerHTML = "X"

        //Apaga o resultado do jogo anterior
        document.getElementById("IndicadorVencedor").innerHTML = ""

        //Zera o numero de jogadas
        cont=0
    }
}

// Verifica todos as combinações possíveis de se ganhar o jogo
function ganhou() {
    //Retorna 1 caso X vença, retorna 2 caso O vença, Retorna 0 caso não haja vencedor
    //caso X vença
    //linhas iguais
    if (document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[1].innerHTML &&
        document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[2].innerHTML &&
        document.getElementsByClassName("casa")[0].innerHTML == "X"){
        return 1;
    }
    else if (document.getElementsByClassName("casa")[3].innerHTML == document.getElementsByClassName("casa")[4].innerHTML &&
        document.getElementsByClassName("casa")[3].innerHTML == document.getElementsByClassName("casa")[5].innerHTML &&
        document.getElementsByClassName("casa")[3].innerHTML == "X"){
        return 1;
    }
    else if (document.getElementsByClassName("casa")[6].innerHTML == document.getElementsByClassName("casa")[7].innerHTML &&
        document.getElementsByClassName("casa")[6].innerHTML == document.getElementsByClassName("casa")[8].innerHTML &&
        document.getElementsByClassName("casa")[6].innerHTML == "X"){
        return 1;
    }
    //colunas iguais
    else if (document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[3].innerHTML &&
        document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[6].innerHTML &&
        document.getElementsByClassName("casa")[0].innerHTML == "X"){
        return 1;
    }
    else if (document.getElementsByClassName("casa")[1].innerHTML == document.getElementsByClassName("casa")[4].innerHTML &&
        document.getElementsByClassName("casa")[1].innerHTML == document.getElementsByClassName("casa")[7].innerHTML &&
        document.getElementsByClassName("casa")[1].innerHTML == "X"){
        return 1;
    }
    else if (document.getElementsByClassName("casa")[2].innerHTML == document.getElementsByClassName("casa")[5].innerHTML &&
        document.getElementsByClassName("casa")[2].innerHTML == document.getElementsByClassName("casa")[8].innerHTML &&
        document.getElementsByClassName("casa")[2].innerHTML == "X"){
        return 1;
    }
    //diagonais iguais
    else if (document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[4].innerHTML &&
        document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[8].innerHTML &&
        document.getElementsByClassName("casa")[0].innerHTML == "X"){
        return 1;
    }
    else if (document.getElementsByClassName("casa")[2].innerHTML == document.getElementsByClassName("casa")[4].innerHTML &&
        document.getElementsByClassName("casa")[2].innerHTML == document.getElementsByClassName("casa")[6].innerHTML &&
        document.getElementsByClassName("casa")[2].innerHTML == "X"){
        return 1;
    }
    //caso O vença
    //linhas iguais
    if (document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[1].innerHTML &&
        document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[2].innerHTML &&
        document.getElementsByClassName("casa")[0].innerHTML == "O"){
        return 2;
    }
    else if (document.getElementsByClassName("casa")[3].innerHTML == document.getElementsByClassName("casa")[4].innerHTML &&
        document.getElementsByClassName("casa")[3].innerHTML == document.getElementsByClassName("casa")[5].innerHTML &&
        document.getElementsByClassName("casa")[3].innerHTML == "O"){
        return 2;
    }
    else if (document.getElementsByClassName("casa")[6].innerHTML == document.getElementsByClassName("casa")[7].innerHTML &&
        document.getElementsByClassName("casa")[6].innerHTML == document.getElementsByClassName("casa")[8].innerHTML &&
        document.getElementsByClassName("casa")[6].innerHTML == "O"){
        return 2;
    }
    //colunas iguais
    else if (document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[3].innerHTML &&
        document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[6].innerHTML &&
        document.getElementsByClassName("casa")[0].innerHTML == "O"){
        return 2;
    }
    else if (document.getElementsByClassName("casa")[1].innerHTML == document.getElementsByClassName("casa")[4].innerHTML &&
        document.getElementsByClassName("casa")[1].innerHTML == document.getElementsByClassName("casa")[7].innerHTML &&
        document.getElementsByClassName("casa")[1].innerHTML == "O"){
        return 2;
    }
    else if (document.getElementsByClassName("casa")[2].innerHTML == document.getElementsByClassName("casa")[5].innerHTML &&
        document.getElementsByClassName("casa")[2].innerHTML == document.getElementsByClassName("casa")[8].innerHTML &&
        document.getElementsByClassName("casa")[2].innerHTML == "O"){
        return 2;
    }
    // diagonais iguais
    else if (document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[4].innerHTML &&
        document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[8].innerHTML &&
        document.getElementsByClassName("casa")[0].innerHTML == "O"){
        return 2;
    }
    else if (document.getElementsByClassName("casa")[2].innerHTML == document.getElementsByClassName("casa")[4].innerHTML &&
        document.getElementsByClassName("casa")[2].innerHTML == document.getElementsByClassName("casa")[6].innerHTML &&
        document.getElementsByClassName("casa")[2].innerHTML == "O"){
        return 2;
    }
    else{
        return 0;
    }
    
}